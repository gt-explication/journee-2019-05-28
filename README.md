# journee-2019-05-28

## Réunion de travail du GT

Le lundi 27 Mai après-midi:

* 16h - Explicabilité et convivialité d’outils de diagnostic médical basé sur de l’IA - Christophe Denis et Judith Nicogossian
* 16h30 - Explicabilité: quels apports pour l’évaluation et la certification? - Agnès Delaborde, LNE
* 17h - Présentation de l’appel CHIST-ERA ERA-NET 2019, dont l’une des thématiques sera “Explainable Machine Learning-based Artificial Intelligence” - Mathieu Girerd (Coordinateur de l’appel)
* 17h30 - Session ouverte et discussion

## Invités et table ronde lors des journées plénières

Le mardi 28 Mai, dans le cadre des journées plénières:

* 14h - Interprétation d’images médicales: quelques travaux vers l’explicabilité - Céline Hudelot (MICS, Centrale Supelec)
* 14h45 - Apprentissage artificiel et interprétabilité pour la médecine de précision - Jean-Daniel Zucker (UMMISCO, IRD, Sorbonne Université)
* 16h - Table ronde: IA et santé: quelle explicabilité? Christophe Denis, Céline Hudelot, Cloderic Mars, Nicolas Maudet, Judith Nicogossian, Jean-Daniel Zucker

